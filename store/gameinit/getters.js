import Cookie from 'js-cookie'

export default {
  async gameBoard(state) {
    if (state.board !== {}) {
      return await JSON.parse(Cookie.get('gameboardinit'))
    }
    return state.board
  }
}
