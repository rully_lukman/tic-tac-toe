export default {
  setGameBoard(context, data) {
    context.commit('SET_GAME_BOARD', data)
  },
  setNewGame(context) {
    context.commit('SET_NEW_GAME')
  },
}
