import Vue from 'vue'
import Cookies from 'js-cookie'

export default {
  SET_GAME_BOARD(state, data) {
    Vue.set(state, 'board', data)
    Cookies.set('gameboardinit', JSON.stringify(data))
  },
  SET_NEW_GAME(state) {
    Vue.set(state, 'board', {})
    Cookies.remove('gameboardinit')
  },
}
