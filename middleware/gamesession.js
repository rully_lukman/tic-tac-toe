import Cookie from 'js-cookie'

export default function ({ store, redirect, route, app }) {
  if (!Cookie.get('gameboardinit')) {
    return redirect('/')
  }
  const gameboard = JSON.parse(Cookie.get('gameboardinit'))
  if (gameboard.singlePlayer && gameboard.boardValue.length !== 0) {
    store.dispatch('gameinit/setNewGame')
    return redirect('/')
  }
}